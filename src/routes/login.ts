import {Request, Response, Router} from 'express';
import log from '@ajar/marker';

const router = Router();


const loginRouterMW = (req : Request,res:Response,next:Function)=>{
    log.blue('I\'m in login router')
    next()
}

router.use(loginRouterMW) // router level middleware


const funcMW = (req : Request,res:Response,next:Function)=>{
    log.blue('I\'m a specific routing function')
    next()
}


router.post('/user',funcMW, (req : Request,res:Response) => {
    const {username, password, age} = req.body
    res.status(200).send(`welcome ${username} to our server`)
})

export default router