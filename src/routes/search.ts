import {Request, Response, Router} from 'express';
import log from '@ajar/marker';

const router = Router();

router.get('/', (req : Request,res:Response) => {
    const {food, town} = req.query
    res.status(200).setHeader('content-type','text/html')
    .send(`<h1>you can find ${food} in ${town} city</h1>`)
})

export default router