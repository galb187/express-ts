import {Request, Response, Router} from 'express';
import log from '@ajar/marker';

const router = Router();

router.get('/:user_id/:username',  (req : Request,res:Response) => {
    const {user_id,username} = req.params;
    res.status(200).send(`username is: ${username} and ID is ${user_id}`)
})

export default router