import express, {Request, RequestHandler, Response} from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import usersRouter from './routes/users.js'
import loginRouter from './routes/login.js'
import searchRouter from './routes/search.js'
// import * as dotenv from "dotenv";

const app = express()

app.use(express.json()) //relevant for req.body
app.use( morgan('dev') );
const {PORT , HOST} = process.env

// const startingMiddleware = (req : Request,res:Response,next:Function)=>{
//     log.blue('this is the first middleware every request is going through')
//     next()
// }

const startingMiddleware : RequestHandler = (req ,res,next)=>{
    log.blue('this is the first middleware every request is going through')
    next()
}

app.use(startingMiddleware) //app middleware

const print : RequestHandler = (req,res,next)=>{
    log.blue('this is printing function')
    next()
}

app.use('/users',usersRouter)
app.use('/login',loginRouter)
app.use('/search',searchRouter)
app.use('/',print)

app.use('*' , (req : Request,res:Response)=>{
    res.status(404).send("-404-Error- Page not found")
})

app.listen(Number(PORT), HOST as string,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});

